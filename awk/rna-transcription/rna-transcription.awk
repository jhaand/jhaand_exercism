BEGIN {
    output_string =  ""
}

NF {
    len = length($0)
    pos = 1
    while (pos <= len) {
        letter = substr($0, pos, 1)

        switch(letter){
        case "C":
            letter = "G"
            break
        case "G":
            letter = "C"
            break
        case "T":
            letter = "A"
            break
        case "A": 
            letter = "U"
            break
        default: 
           print "Invalid nucleotide detected."
           exit 1
       }
 
        output_string = output_string letter
        pos++
        }

    print output_string
}
