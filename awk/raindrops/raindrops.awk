# These variables are initialized on the command line (using '-v'):
# - num
BEGIN {
    output_str = ""

    if ((num % 3) == 0) {
        output_str = output_str "Pling"
        }
    if ((num % 5) == 0) {
        output_str = output_str "Pkang"
        }
    if ((num % 7) == 0) {
        output_str = output_str "Plong"
        }

    if (output_str == "") {
        output_str = num
        }
    print(output_str)
    }
    
