BEGIN {
    score = 0
}

NF {
    len = length ($0)
    word = toupper($0)
    pos = 1

    while (pos <= len) {
        letter = toupper(substr(word, pos, 1))
        switch (letter) {
            case /[AEIOULNRST]/:
                score = score + 1
                break
            case /[GD]/:
                score = score + 2
                break
            case /[BCMP]/:
                score = score + 3
                break
            case /[FHVWY]/:
                score = score + 4
                break
            case /[K]/:
                score = score + 5
                break
            case /[JX]/:
                score = score + 8
                break
            case /[QZ]/:
                score = score + 10
                break
           default: 
                break
            }

            score = score + letter_value
            pos++
        }
        printf("%s", word)
    }

END {
    print "," score
    }
