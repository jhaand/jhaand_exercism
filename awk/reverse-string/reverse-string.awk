BEGIN { input_str = "" }

NF { input_str = $0 }

END {
    pos_str = length(input_str)

    while (pos_str > 0) {
        printf("%s",substr(input_str, pos_str, 1))
        pos_str--
        }
    }

