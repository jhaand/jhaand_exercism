return function(s)
    local letters = {}
    local isalpha = "abcdefghjiklmnopqrstuvwxyz"
    local result = true
    for i = 1,#s do
        local letter = s:sub(i,i):lower()
        if isalpha:find(letter) then
            if letters[letter] == nil then
                letters[letter] = 1
            else
                result = false
            end
        end
    end

    return result
end
