local house = {}

local lines = {
{"horse and the hound and the horn", "belonged to"},
{"farmer sowing his corn", "kept"},
{"rooster that crowed in the morn", "woke"},
{"priest all shaven and shorn", "married"},
{"man all tattered and torn", "kissed"},
{"maiden all forlorn", "milked"},
{"cow with the crumpled horn", "tossed"},
{"dog", "worried"},
{"cat", "killed"},
{"rat", "ate"},
{"malt", "lay in"},
}

house.verse = function(which)
    local output = "This is the "

    if which > 1 then
        for i = #lines-which+2, #lines do
            output = output .. string.format("%s\nthat %s the ", lines[i][1], lines[i][2])
        end
    end

    return output .. "house that Jack built."
end

house.recite = function()
    local output = ""

    for i=1,#lines+1 do
        output = output .. house.verse(i) .. "\n"
    end
    return string.sub(output,1 ,-2) -- Return string without final newline
end

return house
