local BankAccount = {}

function BankAccount:new()
    local account = {amount = 0, open = true}
    self.__index = self
    return setmetatable(account, self)
end

function BankAccount:balance()
    return self.amount
end

function BankAccount:deposit(n)
    assert(n > 0, "Error: Deposit amount is negative")
    assert(self.open, "Error: Account is closed")
    self.amount = self.amount + n
end

function BankAccount:withdraw(n)
    assert(n > 0, "Error: Withdraw amount is negative")
    assert(self.open, "Error: Account is closed")
    assert(self.amount > n, "Error: Account overdrawn")
    self.amount = self.amount - n
end

function BankAccount:close()
    self.open = false
end
return BankAccount
