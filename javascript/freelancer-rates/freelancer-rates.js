// @ts-check
//
// Get those rates calculated!

/** 
    * The number of hours worked per day.
    */
const HOURS_PER_DAY = 8;
/**
 * The day rate, given a rate per hour
 *
 * @param {number} ratePerHour
 * @returns {number} the rate per day
 */
export function dayRate(ratePerHour) {
    return ratePerHour * HOURS_PER_DAY;
}

/**
 * Calculates the number of days in a budget, rounded down
 *
 * @param {number} budget: the total budget
 * @param {number} ratePerHour: the rate per hour
 * @returns {number} the number of days
 */
export function daysInBudget(budget, ratePerHour) {
    return Math.floor(budget / (ratePerHour * HOURS_PER_DAY));
}

/**
 * Calculates the discounted rate for large projects, rounded up
 *
 * @param {number} ratePerHour
 * @param {number} numDays: number of days the project spans
 * @param {number} discount: for example 20% written as 0.2
 * @returns {number} the rounded up discounted rate
 */
export function priceWithMonthlyDiscount(ratePerHour, numDays, discount) {
    const DAYS_PER_MONTH = 22;

    const months_whole = Math.floor(numDays / DAYS_PER_MONTH);
    const months_rest = ((numDays % DAYS_PER_MONTH) / DAYS_PER_MONTH) 
    const months_discount = months_whole * (1 - discount)
    const amount = (months_discount + months_rest) * DAYS_PER_MONTH * HOURS_PER_DAY  * ratePerHour;
    return Math.ceil(amount);
}
