#include "hamming.h"
#include <string.h>

int compute(const char *lhs, const char *rhs) {
    int len_lhs = strlen(lhs);
    int len_rhs = strlen(rhs);
    if (len_lhs != len_rhs) {
        return -1;
    }

    if (len_lhs == 0) {
        return 0;
    }
    int hamming_len = 0;
    int index = 0;

    while (lhs[index]) {
        if (lhs[index] != rhs[index]) {
            hamming_len++;
        }
        index++;
    }
    return hamming_len;
}
