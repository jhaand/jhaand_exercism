#include "grains.h"

uint64_t square(uint8_t index) {
    if ((index < 1) || (index > 64)) {
        return 0;
    }

    uint8_t counter = 1;
    uint64_t square_count = 1;

    while (counter++ < index) {
        square_count *= 2;
    }

    return square_count;
}

uint64_t total(void) {
    uint8_t index = 64;
    uint8_t square_pos = 1;
    uint64_t total = 0;

    while (square_pos <= index) {
        total += square(square_pos);
        square_pos++;
    }

    return total;
}
