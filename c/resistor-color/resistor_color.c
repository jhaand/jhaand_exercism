#include "resistor_color.h"

int color_code(resistor_band_t color) { return color; }

resistor_band_t *colors(void) {
    static resistor_band_t color_values[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    return color_values;
}
