#include "armstrong_numbers.h"
#include <stdio.h>

bool is_armstrong_number(int candidate)
{
    int check = 0;
    const int base = 10;

    int digits = 0;

    // See how much digits the number consists of.
    // If candidate is zero, digits will also become zero. This poses no
    // problem because of the check at the end.
    check = candidate;
    while (check != 0) {
        check /= base;
        digits++;
    }
    // printf("digits: %d\n", digits);

    check = 0;
    int temp = candidate;
    while (temp > 0) {
        int lastnumber = temp % base;
        check +=  pow_int(lastnumber, digits);
        temp /= base;
        // printf("candidate: %d, check: %d, temp: %d\n", candidate, check, temp);
    }

    return (check == candidate) ;
}

// Since math.h works with doubles, lets make our own power function
// with ints.
int pow_int(int base, int exp)
{
    int result = base;

    for (int i = exp; i > 1; i--) {
        result *= base;
    }
    // printf("base: %d, exp: %d, result: %d\n", base, exp, result);
    return result;
}
