#include "isogram.h"
#include <ctype.h>
#include <stddef.h>

bool is_isogram(const char phrase[]) {
    if (phrase == NULL) {
        return false;
    }

    bool alphabet[26] = {false};
    int alphabet_index;

    const char *index = phrase;
    while (*index != '\0') {
        char character = *index++;
        if (!isalpha(character)) {
            continue;
        }
        alphabet_index = tolower(character) - 'a';
        if (alphabet[alphabet_index]) {
            return false;
        } else {
            alphabet[alphabet_index] = true;
        }
    }
    return true;
}
