const SPEED_PER_HOUR: f64 = 221.0;
const EFFICIENCY_FULL: f64 = 1.0;
const EFFICIENCY_MID: f64 = 0.9;
const EFFICIENCY_LOW: f64 = 0.77;

pub fn production_rate_per_hour(speed: u8) -> f64 {
    speed as f64
        * SPEED_PER_HOUR
        * match speed {
            0..=4 => EFFICIENCY_FULL,
            5..=8 => EFFICIENCY_MID,
            9..=10 => EFFICIENCY_LOW,
            _ => 0.0,
        }
}

pub fn working_items_per_minute(speed: u8) -> u32 {
    (production_rate_per_hour(speed) / 60.0) as u32
}
