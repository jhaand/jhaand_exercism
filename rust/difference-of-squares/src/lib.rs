pub fn square_of_sum(n: u32) -> u32 {
    let mut tot: u32 = 0;
    for i in 1..=n {
        tot += i;
    }
    u32::pow(tot, 2)
}

pub fn sum_of_squares(n: u32) -> u32 {
    let mut tot = 0;
    for i in 1..=n {
        tot += u32::pow(i, 2);
    }
    tot
}

pub fn difference(n: u32) -> u32 {
    square_of_sum(n) - sum_of_squares(n)
}
