use std::fmt;

#[derive(Debug)]
pub struct Clock {
    hours: i32,
    minutes: i32,
}

const MINS_PER_DAY: i32 = 24 * 60;

fn mins_in_24h(hours: i32, mins: i32) -> i32 {
    let tot_mins = (hours * 60 + mins) % (MINS_PER_DAY);
    if tot_mins >= 0 {
        return tot_mins;
    } else {
        return tot_mins + MINS_PER_DAY;
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let tot_minutes = mins_in_24h(self.hours, self.minutes);
        let loc_hours = (tot_minutes / 60) % 24;
        let loc_minutes = tot_minutes % 60;

        write!(f, "{:02}:{:02}", loc_hours, loc_minutes)
    }
}

impl PartialEq for Clock {
    fn eq(&self, other: &Self) -> bool {
        let tot_self = mins_in_24h(self.hours, self.minutes);
        let tot_other = mins_in_24h(other.hours, other.minutes);
        tot_self == tot_other
    }
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        Clock { hours, minutes }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Clock {
            hours: self.hours,
            minutes: self.minutes + minutes,
        }
    }
}
