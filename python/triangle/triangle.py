"""
Module to check the sides of a triangle.
"""


def is_triangle(sides):
    """
    Check if this is a valid triangle.
    """
    largest = max(sides)
    return sum(sides) - largest > largest


def equilateral(sides):
    """
    Check if all sides are equal.
    """
    return is_triangle(sides) and sides.count(sides[0]) == 3


def isosceles(sides):
    """
    Check if 2 sides or more are equal.
    """
    return is_triangle(sides) and len(set(sides)) <= 2


def scalene(sides):
    """
    Check if no side equals another side.
    """
    return is_triangle(sides) and len(set(sides)) == 3
