package cars

// CalculateWorkingCarsPerHour calculates how many working cars are
// produced by the assembly line every hour.
func CalculateWorkingCarsPerHour(productionRate int, successRate float64) float64 {
    return float64(productionRate) * (successRate / 100.0)
}

// CalculateWorkingCarsPerMinute calculates how many working cars are
// produced by the assembly line every minute.
func CalculateWorkingCarsPerMinute(productionRate int, successRate float64) int {
    return int(float64(productionRate) * (successRate/ (100.0 * 60.0)))
}

const CostPerNormalCar = 10000
const CostPer10EfficiantCars = 95000
// CalculateCost works out the cost of producing the given number of cars.
func CalculateCost(carsCount int) uint {
    efficientCarCost := (carsCount / 10) * CostPer10EfficiantCars
    normalCarCost := (carsCount % 10) * CostPerNormalCar
    return uint(efficientCarCost + normalCarCost) 
}
