package purchase

// NeedsLicense determines whether a license is needed to drive a type of vehicle. Only "car" and "truck" require a license.
func NeedsLicense(kind string) bool {
    result := true
    if kind == "bike" || kind == "stroller" || kind == "e-scooter" {
        result = false 
    }

    return result
}

// ChooseVehicle recommends a vehicle for selection. It always recommends the vehicle that comes first in lexicographical order.
func ChooseVehicle(option1 string, option2 string) string {
    var best  string
    if option1 <= option2 {
        best = option1
    } else {
        best = option2 
    }
    return best + " is clearly the better choice."

}

// CalculateResellPrice calculates how much a vehicle can resell for at a certain age.
func CalculateResellPrice(originalPrice, age float64) float64 {
    var factor float64
    
    if age < 3 { 
        factor = 0.80
    } else if age < 10 {
        factor = 0.70 
    } else { 
        factor = 0.50
    }
    
    return originalPrice * factor
}
