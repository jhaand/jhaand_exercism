package techpalace

import "strings"

// WelcomeMessage returns a welcome message for the customer.
func WelcomeMessage(customer string) string {
    return "Welcome to the Tech Palace, " + strings.ToUpper(customer)
}

// AddBorder adds a border to a welcome message.
func AddBorder(welcomeMsg string, numStarsPerLine int) string {
    header := strings.Repeat("*", numStarsPerLine)
    return header + "\n" + welcomeMsg + "\n" + header
}

// CleanupMessage cleans up an old marketing message.
func CleanupMessage(oldMsg string) string {
    message := strings.Split(oldMsg, "\n")[1]
    message = strings.TrimSpace(strings.Replace(message, "*", "", -1))
    return message
}
