#!/usr/bin/env bash

set -f 

acronym=""
words=$(echo "$@" | sed -r 's/[-_\\*,]/ /g') 

for word in $words; do 
    acronym+="$( echo ${word:0:1} | tr '[:lower:]' '[:upper:]')"
done

echo $acronym

