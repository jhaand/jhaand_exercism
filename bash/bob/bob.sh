#!/usr/bin/env bash

set -f   # No filename expansion.

# Initialize variables.
question=false
yelling=false
silence=false

sentence=$( echo "$@" | sed -e 's/[\n\r\t]//g' | awk '{$1=$1;print}') 
## Detect aspects of the input.
# Check for question.
if [[ $sentence =~ \?\ *$  ]] ; then
    question=true
fi

# Check for yelling
if [[ "$sentence" =~ [a-z]+ ]] || ! [[ $sentence =~ [A-Z] ]] ; then
    yelling=false
else
    yelling=true
fi

# check for silence
if [[ -z $sentence ]] ; then
    silence=true
fi

## Choose what to say.
if  $silence  ; then
    echo "Fine. Be that way!"
elif ( $yelling  && $question ) ; then
    echo "Calm down, I know what I'm doing!"
elif $yelling ; then
    echo "Whoa, chill out!"
elif $question  ; then
    echo "Sure."
else
    echo "Whatever."
fi

