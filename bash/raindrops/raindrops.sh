#!/usr/bin/env bash

(( $1 % 3 )) || OUTPUT="Pling"
(( $1 % 5 )) || OUTPUT+="Plang"
(( $1 % 7 )) || OUTPUT+="Plong"

if [[ -z $OUTPUT ]]; then
    OUTPUT=$1
fi

echo $OUTPUT
