module difference_of_squares
    implicit none
contains

    integer function square_of_sum(n) result(squares)
        integer :: n, i
        squares = 0

        do i=1,n
            squares = squares + i
        end do
        squares = squares**2
    end function square_of_sum

    integer function sum_of_squares(n) result (sums)
        integer :: n, i
        sums = 0
        do i=1,n
            sums = sums + i**2
        end do

    end function sum_of_squares

    integer function difference(n) 
        integer :: n 

        difference = square_of_sum(n) - sum_of_squares(n)
    end function difference

end module difference_of_squares
