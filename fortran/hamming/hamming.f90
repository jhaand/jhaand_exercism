module hamming
    implicit none
contains
    function compute(strand1, strand2, distance)
        character(*) :: strand1, strand2
        integer :: distance, index, len1, len2
        logical :: compute, check

        compute = .false.
        check = .false.
        distance = 0

        len1 = len(strand1)
        len2 = len(strand2)
        if (len1 .eq. len2)  then
            compute = .true.
            do index = 1,len(strand1)
                if (strand1(index:index) /=  strand2(index:index)) then
                    distance = distance + 1
                end if
            end do
        end if
    end function compute

end module hamming
