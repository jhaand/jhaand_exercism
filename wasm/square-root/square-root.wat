(module
  ;;
  ;; Return the square root of the given number.
  ;;
  ;; @param {i32} radicand
  ;;
  ;; @returns {i32} square root of radicand
  ;;
  (func (export "squareRoot") (param $radicand i32) (result i32)
        (local $sqrt i32)
        (local $tmp i32)
        (local.set $sqrt (i32.const 0))

        (loop $loop 
              local.get $sqrt
              i32.const 1
              i32.add
              local.set $tmp

              local.get $tmp
              local.get $tmp
              i32.mul
              local.get $radicand
              i32.gt_s
              (if (then 
                    local.get $sqrt
                    return) 
                (else 
                  local.get $sqrt
                  i32.const 1
                  i32.add
                  local.set $sqrt
                  br $loop))


              )
        (return (i32.const 42))
        )
  )
