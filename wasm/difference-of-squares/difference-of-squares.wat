(module
  ;;
  ;; Calculate the square of the sum of the first N natural numbers
  ;;
  ;; @param {i32} max - The upper bound (inclusive) of natural numbers to consider
  ;;
  ;; @returns {i32} The square of the sum of the first N natural numbers
  ;;
  (func $squareOfSum (export "squareOfSum") (param $max i32) (result i32)
        (local $total i32)
        (local.set $total (i32.const 0))

        (loop  $loop
               ;; Increase $total with $max
               (local.set $total (i32.add (local.get $total) (local.get $max)))
               ;; Decrease $max with 1
               (local.set $max (i32.sub (local.get $max) (i32.const 1)))

               ;; Check if $max has become 0
               (if (i32.eqz (local.get $max))
                 ;; In that case fall through of the loop and square the result. 
                 (local.set $total (i32.mul (local.get $total) (local.get $total)) ) 
                 ;; If not 0, then branch to the beginning.
                 (br $loop))
               )
        (return (local.get $total))
        )

  ;;
  ;; Calculate the sum of the squares of the first N natural numbers
  ;;
  ;; @param {i32} max - The upper bound (inclusive) of natural numbers to consider
  ;;
  ;; @returns {i32} The sum of the squares of the first N natural numbers
  ;;
  (func $sumOfSquares (export "sumOfSquares") (param $max i32) (result i32)
        (local $total i32)
        (local.set $total (i32.const 0))

        (loop  $loop
               ;; Increase $total with $max squared
               (local.set $total (i32.add (local.get $total) 
                                          (i32.mul (local.get $max)(local.get $max))
                                          ))
               ;; Decrease $max with 1
               (local.set $max (i32.sub (local.get $max) (i32.const 1)))

               ;; Check if $max has become 0
               (if (i32.eqz (local.get $max))
                 ;; In that case fall through of the loop. 
                 (nop )
                 ;; If not 0, then branch to the beginning.
                 (br $loop))
               )

        (return (local.get $total))
        )

  ;;
  ;; Calculate the difference between the square of the sum and the sum of the 
  ;; squares of the first N natural numbers.
  ;;
  ;; @param {i32} max - The upper bound (inclusive) of natural numbers to consider
  ;;
  ;; @returns {i32} Difference between the square of the sum and the sum of the
  ;;                squares of the first N natural numbers.
  ;;
  (func (export "difference") (param $max i32) (result i32)
        (i32.sub 
          (call $squareOfSum (local.get $max))
          (call $sumOfSquares (local.get $max)))
        )
  )
