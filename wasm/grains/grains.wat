(module
  ;;
  ;; Calculate the number of grains of wheat on the nth square of the chessboard
  ;;
  ;; @param {i32} squareNum - The square of the chessboard to calculate the number of grains for
  ;;
  ;; @returns {i64} - The number of grains of wheat on the nth square of the 
  ;;                  chessboard or 0 if the squareNum is invalid. The result
  ;;                  is unsigned.
  ;;
  (func $square (export "square") (param $squareNum i32) (result i64)
        (if (i32.or
              (i32.le_s (local.get $squareNum) (i32.const 0))  
              (i32.ge_s (local.get $squareNum) (i32.const 65)) 
              )
          (return (i64.const 0)) 
          )

        (i64.shl (i64.const 1) 
                 (i64.extend_i32_s (i32.sub 
                                     (local.get $squareNum) (i32.const 1))))
        return
        )

  ;;
  ;; Calculate the sum of grains of wheat acrosss all squares of the chessboard
  ;;
  ;; @returns {i64} - The number of grains of wheat on the entire chessboard.
  ;;                  The result is unsigned.
  ;;

  ;; A signed 2-complement number of -1 equals (maxint - 1) for an unsigned number
  ;; Since the chess board has 64 squares, all the bits have become 1. 
  (func (export "total") (result i64)
        i64.const -1 
        return
        )
  )
