(module
  (func (export "steps") (param $number i32) (result i32)
        (local $step i32)
        (local.set $step (i32.const 0))

        local.get $number 
        i32.const 0
        i32.le_s
        (if (then 
              i32.const -1
              return))

        (loop $loop
              ;; Check if $number is already one or lower 
              local.get $number
              i32.const 1
              i32.gt_s
              (if  
                (then
                  ;; Check if number is uneven 
                  local.get $number
                  i32.const 2
                  i32.rem_s
                  (if 
                    ;; If number is uneven, then multiply by 3 and add 1. 
                    (then
                      local.get $number
                      i32.const 3
                      i32.mul
                      i32.const 1
                      i32.add
                      local.set $number

                      )
                    ;; If number is even divide by two
                    (else
                      local.get $number
                      i32.const 2
                      i32.div_s
                      local.set $number
                      ) 
                    )
                  ;; increase $step by 1
                  i32.const 1
                  local.get $step
                  i32.add
                  local.set $step

                  ;; Branch back to the beginning.
                  br $loop
                  ) (else 
                  ;; Break out of the loop
                  )
                ) 
              )
        local.get $step 
        return 
        )
  )
