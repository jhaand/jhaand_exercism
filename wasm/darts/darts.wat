(module
  (func (export "score") (param $x f32) (param $y f32) (result i32)
  (local $range f32)
  ;; Calculate the square of the range the dart hit the board.
  (local.set $range (f32.sqrt (f32.add 
        (f32.mul (local.get $x) (local.get $x)) 
        (f32.mul (local.get $y) (local.get $y))
        )))
;; Range^2 less or equal than 1.0 -> 10 points. 
  local.get $range 
  f32.const 1.0 
  f32.le 
  (if (then 
    (return (i32.const 10))
  ))
;; Range^2 less or equal than 25.0 -> 5 points. 
  local.get $range 
  f32.const 5.0 
  f32.le 
  (if (then 
    (return (i32.const 5))
  ))
;; Range^2 less or equal than 100 -> 1 point. 
  local.get $range 
  f32.const 10.0 
  f32.le 
  (if (then 
    (return (i32.const 1))
  ))
;; Otherwise 0 points.
(return (i32.const 0))
  )
)
