<?php

class HighSchoolSweetheart
{
    public function firstLetter(string $name): string
    {
        return mb_str_split($name)[0];
    }

    public function initial(string $name): string
    {
        return strtoupper(mb_str_split($name)[0].'.');
    }

    public function initials(string $name): string
    {
        $names = mb_split(' ', $name);
        $output_initials = $this->initial($names[0]);

        unset($names[0]);
        foreach ($names as $split_name) {
            $output_initials .= ' ' .$this->initial($split_name) ;
        }
        return $output_initials;
    }

    public function pair(string $sweetheart_a, string $sweetheart_b): string
    {
        $initial_a = $this->initials($sweetheart_a);
        $initial_b = $this->initials($sweetheart_b);
        
        $love = str_pad($initial_a . '  +  ' . $initial_b, 23, ' ', STR_PAD_BOTH) ;
        $output= <<<OUTPUT_HEART
                 ******       ******
               **      **   **      **
             **         ** **         **
            **            *            **
            **                         **
            ** $love **
             **                       **
               **                   **
                 **               **
                   **           **
                     **       **
                       **   **
                         ***
                          *
            OUTPUT_HEART;
        return $output;
    }
}
