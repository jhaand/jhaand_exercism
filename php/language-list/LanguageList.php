<?php

function language_list()
{
    if (func_num_args() > 0) {
        $lang = func_get_args();
    } else {
        $lang = [];
    }
    
    return $lang;
}

function add_to_language_list($lang, $new_lang) {
    $lang[] =  $new_lang;
    return $lang;
}

function prune_language_list($lang) {
    array_shift($lang);
    return $lang;
}

function current_language($lang) {
    return $lang[0];
}

function language_list_length($lang) {
    return count($lang);
}
