<?php

declare(strict_types=1);

class Lasagna
{
    private $cooktime = 40;
    private $layertime = 2;

    public function expectedCookTime() {
        return $this->cooktime;
    }

    function remainingCookTime($elapsed_minutes) { 
        return $this->cooktime - $elapsed_minutes;
    }

    function totalPreparationTime($layers_to_prep) {
        return $this->layertime * $layers_to_prep;
    }

    function totalElapsedTime($layers_to_prep, $elapsed_minutes) {
        return $this->layertime * $layers_to_prep + $elapsed_minutes;
    }

    function alarm() {
        return "Ding!";
    }
}
